#include <iostream>
#include <string>

using namespace std;

int main()
{
  int n, k;
  string l1, l2;
  cin >> n;
  while(n-- > 0)
  {
    bool isDif = false;
    cin >> k;
    k--;
    cin >> l1;
    while(k-- > 0)
    {
      cin >> l2;
      if(!isDif)
        if(l1 != l2)
          isDif = true;
    }
    if(isDif) cout << "ingles" << endl;
    else cout << l1 << endl;
  }
  return 0;
}