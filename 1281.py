# -*- coding: utf-8 -*-

n = int(input())
for x in range(n):
  n_itens = int(input())
  feira = dict()
  price = 0.0
  for i in range(n_itens):
    i = input().split()
    feira[i[0]] = float(i[1])
  n_itens = int(input())
  for i in range(n_itens):
    l = input().split()
    price += feira[l[0]] * int(l[1])
  print('R$ %.2f' % price)