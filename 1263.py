while True:
    try:
        counter = 0
        charCounter = 1
        words = [w for w in input().split()]
        for i in range(len(words) - 1):
            if words[i][0].lower() == words[i+1][0].lower():
                charCounter += 1
            else:
                if charCounter > 1: 
                    counter += 1
                charCounter = 1
        if charCounter > 1 : counter += 1
        print(counter)
    except EOFError:
        break