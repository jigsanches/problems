// STATUS: ACCEPTED
#include <bits/stdc++.h>
#define ll long long
using namespace std;


int main()
{
    // ios_base::sync_with_stdio(false);
    // cin.tie(NULL);

    int n;
    string s;
    cin >> n;
    cin.ignore(1);
    while(n-- > 0)
    {
        getline(cin, s);
        string o;
        auto l = s.length() - 1;
        for(int i = 0; i <= l; ++i)
        {
            if(s[i] == '0')
            {
                int j = i+1;
                
                while(j <= l && s[j] == '0' && (j-i) < 255)
                {
                    j++;
                }

                int q = j - i;
                if(q > 2)
                {
                    i += q - 1;
                    o += '#';
                    o += char(q);
                }
                else
                    o += '0';
            }
            else if(s[i] == 0x20)
            {
                int j = i+1;
                
                while(j <= l && s[j] == 0x20 && (j-i) < 255)
                {
                    j++;
                }
                int q = j - i;
                // std::cout << "Found ws seq of size" << q << "\n";
                if(q > 2)
                {
                    i += q - 1;
                    o += '$';
                    o += char(q);
                }
                else
                    o += ' ';
            }
            else
            {
                o += s[i];
            }
        }

        std::cout << o << '\n';
    }
    return 0;
}
