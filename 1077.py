# -*- coding: utf-8 -*-

n = int(input())
# guardamos a precedencia dos operadores
pre = {
    ')' : 0,
    '(' : 0,
    '^' : 3,
    '*' : 2,
    '/' : 2,
    '+' : 1,
    '-' : 1,
    }
for k in range(n):
  stack = []
  result = []
  for c in input():
    if c in pre:
      # eh um operador
      if c == '(': stack.append('(')
      elif c == ')':
        while stack[-1] != '(': result.append(stack.pop())
        stack.pop()
      else:
        while len(stack) > 0 and pre[stack[-1]] >= pre[c] : result.append(stack.pop())
        stack.append(c)
    else:
      result.append(c)
  while len(stack) > 0:
    result.append(stack.pop())
  print(''.join(result))