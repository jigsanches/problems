#include <bits/stdc++.h>
using namespace std;
// STATUS: ACCEPTED
struct edge{
    int end, cap_residual;
    edge(int end, int cap): end(end), cap_residual(cap)
    {}
};
// using edge = pair<int, int>; // edge = (flow, capacity)

int remaining_games[40][40]; // remaining games of (i, j)

bool bfs(vector<vector<edge>>& graph, int parent[]){
    vector<bool> visited(graph.size(), false);
    visited[0] = true;
    parent[0] = -1;
    queue<int> q;
    q.push(0);
    // printf("starting search...\n");
    while(!q.empty()){
        auto v = q.front(); q.pop();
        for(auto e: graph[v]){
            if(!visited[e.end] && e.cap_residual > 0){
                q.push(e.end);
                visited[e.end] = true;
                parent[e.end] = v;
            }
        }
    }
    return visited[1];
}

void print_graph(vector<vector<edge>>& g){
    for(int i = 0; i < g.size(); ++i){
        cout << "vertex i=" << i << endl;
        for(auto e : g[i]){
            printf("(%d, %d) ", e.end, e.cap_residual);
        }
        printf("\n");
    }
}

int main(){
    ios_base::sync_with_stdio(false); // fast IO
    cin.tie(NULL);
    int n, m, g;
    int u, v;
    char c;
    scanf("%d%d%d", &n, &m, &g);
    while(n != 0)
    {
        bool loses = false;
        int target_flow = 0;
        // [0] is source, [1] is sink
        vector<vector<edge>> graph(n + 1, vector<edge>());
        int _size = graph.size();
        int _games = m*2;
        int score[n] = {0};
        for(int i = 0; i < n; ++i){
            for(int j = i+1; j < n; ++j){
                remaining_games[i][j] = remaining_games[j][i] = _games;
            }
            remaining_games[i][i] = 0;
        }
        for(int i = 0; i < g; ++i){ // games
            scanf("%d %c %d", &u, &c, &v);
            if(c == '='){
                score[u]++;
                score[v]++;
            }
            else{
                score[v] += 2;
            }
            remaining_games[u][v] -= 2;
            remaining_games[v][u] -= 2;
        }
        for(int j = 1; j < n; ++j){
            score[0] += remaining_games[0][j]; // score[0] = w_0 + r_0
        }
        for(int j = 1; j < n; ++j){ // arestas dos times
            graph[j+1].push_back(edge{1, score[0] - score[j] - 1});
            graph[1].push_back(edge{j+1, 0});
        }
        for(int i = 1; i < n; ++i){
            for(int j = i+1; j < n; ++j){
                if(remaining_games[i][j] > 0){ // r[i][j] > 0
                    target_flow += remaining_games[i][j];
                    graph[0].push_back({
                        _size, //end
                        remaining_games[i][j] // capacity
                    });
                    graph.emplace_back(
                        vector<edge>{
                            edge{0,0}, // residual edge
                            edge{i+1, INT_MAX}, // forward edges to team layer
                            edge{j+1, INT_MAX}
                        }
                    );
                    // back edges
                    graph[i+1].push_back(
                        edge{_size, 0}
                    );
                    graph[j+1].push_back(
                        edge{_size, 0}
                    );
                    _size++;
                }
            }
        }

        int max_flow = 0;
        int parent[graph.size()];
        while(bfs(graph, parent)){
            // printf("found aug path\n");
            auto path_flow = INT_MAX;
            vector<pair<int,int>> path, r_path;
            v = 1;
            while(v != 0){
                u = parent[v];
                for(int i = 0; i < graph[u].size(); ++i){
                    if(graph[u][i].end == v)
                    {
                        path_flow = min(path_flow, graph[u][i].cap_residual);
                        path.push_back(pair<int, int>(u, i));
                        // printf(" [x] %d %d is in path\n", u, v);
                    }
                }
                for(int i = 0; i < graph[v].size(); ++i){
                    if(graph[v][i].end == u)
                        r_path.push_back(pair<int,int>(v, i));
                }
                v = parent[v];
            }
            // printf("%d path_flow\n", path_flow);
            for(int i = 0; i < path.size(); ++i){
                auto p = path[i];
                auto r_p = r_path[i];
                graph[p.first][p.second].cap_residual -= path_flow;
                graph[r_p.first][r_p.second].cap_residual += path_flow;
            }
            max_flow += path_flow;
        }
        // printf("max flow = %d\n", max_flow);
        for(int i = 1; i < n;++i){ // se score eh menor, nao tem como ganhar
            if(score[0] <= score[i]){
                loses = true; break;
            }
        }
        if(loses){
            printf("N\n");
        }
        else{
            // printf("MAXFLOW=%d\n", max_flow);
            if(target_flow == max_flow)
                printf("Y\n");
            else
                printf("N\n");
        }
        scanf("%d%d%d", &n, &m, &g);
    }
    return 0;
}