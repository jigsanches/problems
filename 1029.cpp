#include <bits/stdc++.h>
// STATUS: ACCEPTED
using namespace std;

using uint = unsigned int;

int main(){
    uint n, j;
    uint fibs [40] = {0, 1, 1, 2, 3,  5};
    uint calls[40] = {1, 1, 3, 5, 9, 15};
    for(int i = 6; i < 40; ++i){
        fibs[i] = fibs[i-1] + fibs[i-2];
        calls[i] = calls[i-1] + calls[i-2] + 1;
    }
    cin >> n;
    while(n-->0){
        cin >> j;
        printf("fib(%u) = %u calls = %u\n", j, calls[j] - 1, fibs[j]);
    }
    return 0;
}