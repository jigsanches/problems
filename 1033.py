count = 0

def fib(n):
    global count
    count += 1
    if n == 0: return 0
    if n == 1: return 1
    return fib(n-1) + fib(n-2)

n, b = [int(i) for i in input().split()]

while n != 0 and b != 0:
    count = 0
    fib(n)
    print(count)
    n, b = [int(i) for i in input().split()]