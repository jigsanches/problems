# -*- coding: utf-8 -*-

n = int(input())
even = []
odd = []
for i in range(n):
  v = int(input())
  if v % 2 == 0: even.append(v)
  else: odd.append(v)
even.sort()
odd.sort(reverse=True)
for i in range(len(even)):
  print(even[i])
for i in range(len(odd)):
  print(odd[i])