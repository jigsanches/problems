#include <bits/stdc++.h>
// STATUS: ACCEPTED
using namespace std;

int main(){
    ios_base::sync_with_stdio(false); // fast IO
    cin.tie(NULL);
    int n, p, toys[100], weight[100];
    cin >> n;
    int dp[101][51];
    while(n --> 0) {
        cin >> p;
        for(int i = 0; i < p; ++i)
            cin >> toys[i] >> weight[i];

        for(int i = 0; i <= p; ++i) {
        // if target is 0 any subset can sum up to 0
            dp[i][0] = 0;
        }
        for(int i = 1; i <= 50; ++i) {
        // with no elements we can never sum up to anything
            dp[0][i] = 0;
        }
        for(int i = 1; i <= p; ++i){
            for(int j = 1; j <= 50; ++j){
                dp[i][j] = weight[i-1] > j ? dp[i-1][j] : 
                    max(toys[i-1] + dp[i-1][j-weight[i-1]], dp[i-1][j]);
            }
        }
        int total_weight = 0;
        int pkt_left = p;
        int n_toys = dp[p][50];
        int j = 50;
        for(int i = p; p > 0 && n_toys > 0; --i){
            if(n_toys == dp[i-1][j])
                continue;
            else{
                j -= weight[i-1];
                total_weight += weight[i-1];
                pkt_left--;
                n_toys -= toys[i-1];
            }
        }
        cout << dp[p][50] << " brinquedos\n" << "Peso: " << total_weight << " kg\n" << "sobra(m) " << pkt_left << " pacote(s)\n\n";
    }
    return 0;
}