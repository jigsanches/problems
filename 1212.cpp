#include <bits/stdc++.h>
// STATUS: ACCEPTED
using namespace std;
using uint = unsigned int;

int main(){
    uint a, b;
    cin >> a >> b;
    while(true){
        if(a == 0 && b == 0) break;
        int count = 0;
        int carry = 0;
        while(true){
            int r1 = a % 10;
            int r2 = b % 10;
            auto sum = r1 + r2 + carry;
            carry = sum > 9 ? 1 : 0;
            if(sum > 9) count++;
            a /= 10;
            b /= 10;
            if(a == 0 && b == 0) break;
        }
        if(count == 1) 
            printf("1 carry operation.\n");
        else if(count > 1)
            printf("%d carry operations.\n", count);
        else
            printf("No carry operation.\n");
        cin >> a >> b;
    }
    return 0;
}