#include <bits/stdc++.h>
// STATUS: ACCEPTED
using namespace std;

using ll = long long;
int main(){
    ios_base::sync_with_stdio(false); // fast IO
    cin.tie(NULL);
    int n, k;
    cin >> n >> k;
    ll n_subs[100][100];
    int nums[100];
    while(n != 0 && k != 0) {
        for(int i = 0; i < n; ++i){
            cin >> nums[i];
            n_subs[0][i] = 1;
        }
        if(n == k)
            cout << 1 << "\n";
        else if(k == 1)
            cout << n << "\n";
        else {
            for(int i = 1; i < k; ++i){ // for each subsequence size
                for(int j = i; j < n; ++j){ // for each subsequence of size i+1 ending at arr[j]
                    n_subs[i][j] = 0;
                    for(int l = i-1; l < j; ++l){ // for each arr[l] where l < j
                        if(nums[j] > nums[l])
                            // every subsequence ending at arr[l] can 
                            // be extended to end at arr[j]
                            n_subs[i][j] += n_subs[i-1][l];
                    }
                }
            }
            // last line has number of sbsquences ending at every arr[j]
            ll n_subsequences_of_size_k = 0;
            for(int j = k-1; j < n; ++j)
                n_subsequences_of_size_k += n_subs[k-1][j];
            cout << n_subsequences_of_size_k << "\n";
        }
        cin >> n >> k;
    }
    return 0;
}