#include <iostream>
#include <math.h>
// STATUS: ACCEPTED
using namespace std;

int main(){
    int n;
    cin >> n;
    long long unsigned int v[100];
    v[0] = 1;
    for(int i = 1; i < 100; ++i)
        v[i] = v[i-1] + ((i+1)*(i+1));
    while(n != 0)
    {
        cout << v[n-1] << endl;
        cin >> n;
    }
    return 0;
}