import 'dart:io';
import 'dart:core';

void main() {
  var maiorAB = (int a, int b) => (a + b + (a-b).abs()) ~/ 2;
  var maiorABC = (int a, int b, int c) => maiorAB(maiorAB(a, b), c);

  var line = stdin.readLineSync()!.split(' ').map(int.parse).toList();
  var maior = maiorABC(line[0], line[1], line[2]);
  print('${maior} eh o maior.');
}