# -*- coding: utf-8 -*-

n, m = (int(i) for i in input().split())
d = dict()
for i in range(n):
  l = input().split()
  d[l[0]] = int(l[1])
for i in range(m):
  v = 0
  l = input()
  while l != ".":
    for word in l.split():
      try:
        v += d[word]
      except:
        pass
    l = input()
  print(v)