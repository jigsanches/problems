// STATUS: ACCEPTED

#include <bits/stdc++.h>

using namespace std;

// Definition for singly-linked list.
struct ListNode {
    int val;
    ListNode *next;
    ListNode() : val(0), next(nullptr) {}
    ListNode(int x) : val(x), next(nullptr) {}
    ListNode(int x, ListNode *next) : val(x), next(next) {}
};

class Solution {
public:
    ListNode* mergeKLists(vector<ListNode*>& lists) {
        auto k = lists.size();

        if(k == 0)
            return nullptr;
        
        ListNode* ans = nullptr, *curr = nullptr;
        vector<ListNode*> starts{k, nullptr};
        for(size_t i = 0; i < k; ++i)
            starts[i] = lists[i];

        auto getMin = [&]()
        {
            ListNode* min = nullptr;
            int m_idx = -1;
            for(size_t i = 0; i < k; ++i)
            {
                auto e = starts[i];
                if(e != nullptr)
                    if((min != nullptr && e->val < min->val) || (min == nullptr))
                    {
                        m_idx = i;
                        min = e;
                    }
            }
            return m_idx;
        };
        
        if(getMin() == -1)
            return nullptr;

        auto m_idx = getMin();
        ans = curr = starts[m_idx];
        starts[m_idx] = starts[m_idx]->next;
        cout << ans->val << ' ';

        while(m_idx = getMin(), m_idx != -1)
        {
            curr->next = starts[m_idx];
            starts[m_idx] = starts[m_idx]->next;
            curr = curr->next;
            cout << curr->val << ' ';
        }

        return ans;
    }
};

int main()
{
    vector<ListNode*> lists{{
        new ListNode(1, new ListNode(4, new ListNode(5))),
        new ListNode(1, new ListNode(3, new ListNode(4))),
        new ListNode(2, new ListNode(6))
    }};

    Solution().mergeKLists(lists);
    return 0;
}