// STATUS: ACCEPTED

#include <bits/stdc++.h>

using namespace std;

// Definition for singly-linked list.
struct ListNode {
    int val;
    ListNode *next;
    ListNode() : val(0), next(nullptr) {}
    ListNode(int x) : val(x), next(nullptr) {}
    ListNode(int x, ListNode *next) : val(x), next(next) {}
};

class Solution {
public:
    ListNode* mergeKLists(vector<ListNode*>& lists) {
        auto k = lists.size();
        
        if(k == 0)
            return nullptr;
        
        auto cmp = [](ListNode* a, ListNode* b){
            return a->val > b->val;
        };
        priority_queue<ListNode*, vector<ListNode*>, decltype(cmp)> pq{cmp};

        for(size_t i = 0; i < k; ++i)
            if(lists[i] != nullptr)
                pq.push(lists[i]);
        
        if(pq.empty())
            return nullptr;

        ListNode* ans, *curr;
        ans = curr = pq.top(); pq.pop();

        if(ans->next != nullptr)
            pq.push(ans->next);

        while(!pq.empty())
        {
            auto min = pq.top(); pq.pop();
            curr->next = min;
            curr = min;

            if(min->next != nullptr)
                pq.push(min->next);
        }

        return ans;
    }
};

int main()
{
    vector<ListNode*> lists{{
        new ListNode(1, new ListNode(4, new ListNode(5))),
        new ListNode(1, new ListNode(3, new ListNode(4))),
        new ListNode(2, new ListNode(6))
    }};

    Solution().mergeKLists(lists);
    return 0;
}