#include <bits/stdc++.h>

using namespace std;

class Solution {
public:
    int lengthOfLongestSubstring(string s) {
        bitset<256> bs(0);
        size_t start, len;
        start = len = 0;

        for(size_t i = 0; i < s.length(); ++i)
        {
            size_t code = s[i];
            if(bs[code]) // if char repeated
            {
                // remove the chars [start, ... , code] from bs
                while (bs[code])
                {
                    bs.reset(size_t(s[start]));
                    start++;
                }
            }
            bs.set(code, true); // sets code as used char
            len = max(bs.count(), len);
        }
        return len;
    }
};

int main()
{
    return 0;
}