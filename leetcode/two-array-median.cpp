// STATUS: ACCEPTED

#include <bits/stdc++.h>

using namespace std;

class Solution {
public:
    double findMedianSortedArrays(vector<int>& nums1, vector<int>& nums2) {
        auto m = nums1.size();
        auto n = nums2.size();

        if(m > n) // ensure m <= n
            return findMedianSortedArrays(nums2, nums1);

        int l = 0, r = m, i, j;
        while(l <= r)
        {
            i = (l + r)/2;
            j = (m + n + 1)/2 - i;

            if(i < m && nums2[j-1] > nums1[i])
                l = i+1;
            else if(i > 0 && nums1[i-1] > nums2[j])
                r = i-1;
            else
            {
                int ml, mr;
                if(i == 0)
                    ml = nums2[j-1];
                else if(j == 0)
                    ml = nums1[i-1];
                else
                    ml = max(nums1[i-1], nums2[j-1]);
                
                if((m+n)%2)
                    return ml;

                if(i == m)
                    mr = nums2[j];
                else if(j == n)
                    mr = nums1[i];
                else
                    mr = min(nums1[i], nums2[j]);
                
                return (ml + mr) / 2.0;
            }
        }
        return numeric_limits<double>::infinity();
    }
};

int main()
{
    vector<int> A{{1, 3}}, B{{2}};
    auto s = Solution();
    cout << s.findMedianSortedArrays(A, B) << '\n';

    return 0;
}