// STATUS: ACCEPTED

#include <bits/stdc++.h>

using namespace std;

int main()
{
typedef vector<int> vi;
typedef pair<int, int> pi;
typedef vector<pi> vp;
    vi input = {3,3};
    int target = 6;

    vp v;
    v.resize(input.size());
    for(int i = 0; i < input.size(); i++)
        v[i] = pi{input[i], i};
    sort(v.begin(), v.end());
    
    vi ans;
    auto last = v.end();
    for(auto& el: v)
    {
        auto x = lower_bound(v.begin(), v.end(), pi(target - el.first, 0), [&](const pi& a, const pi& b){return a.first < b.first;});
        if((x != last) && (x->first + el.first == target))
        {
            ans = vi{{el.second, x->second}};
        }
    }
    sort(ans.begin(), ans.end());
    cout << ans[0] << ' ' << ans[1] << '\n';
    return 0;
}