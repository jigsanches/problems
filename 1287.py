# -*- coding: utf-8 -*-

while True:
  try:
    num = ''
    err = False
    for c in input():
      if c == 'l': num += '1'
      elif c == 'o' or c == 'O': num += '0'
      elif c.isalpha():
        err = True
        break
      elif c.isnumeric(): num += c
    if len(num) == 0: err = True
    elif int(num) > 2147483647: err = True
    if(not err):
      print(int(num))
    else:
      print("error")
  except EOFError:
    break