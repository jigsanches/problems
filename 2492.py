n = 1
while n != 0:
    n = int(input())
    if n == 0: break
    d = dict()
    d_ = dict()
    err = False
    msg = 'Invertible.'
    for i in range(n):
        x, y = input().split(' -> ')
        if d.get(x) != None:
            err = True
            msg = 'Not a function.'
        if not err:
            if d_.get(y) != None:
                msg = 'Not invertible.'
        d[x] = y
        d_[y] = x
    print(msg)
