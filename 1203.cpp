#include <bits/stdc++.h>
// STATUS: ACCEPTED
using namespace std;

int main(){
  int n, k;
  int l, m;
  while(cin >> n){
    cin >> k;
    int regions[n] = { 0 };
    for(int i = 0; i < k; ++i){
        cin >> l >> m;
        regions[l-1]++;
        regions[m-1]++;
    }
    bool sums[n+1][k+1];
    for(int i = 0; i <= n; ++i) {
      // if target is 0 any subset can sum up to 0
      sums[i][0] = true;
    }
    for(int i = 1; i <= k; ++i) {
      // with no elements we can never sum up to anything
      sums[0][i] = false;
    }
    for(int i = 1; i <= n; ++i) {
      for(int j = 1; j <= k; ++j) {
        if(regions[i-1] > j){
          // already exceeded sum, check if arr[0...i-1] can do it
          sums[i][j] = sums[i-1][j];
        }
        else {
          // 2 cases:
          //  -> if arr[0...i-1] can do it, so does arr[0...i]
          //  -> if arr[0...i-1] can do it with (target = j - arr[i]) so 
          //    does arr[0...i].
          sums[i][j] = sums[i-1][j] || sums[i-1][j - regions[i-1]];
        }
      }
    }
    if(sums[n][k]) cout << "S" << endl;
    else cout << "N" << endl;
  }
  return 0;
}