// STATUS: STUPID ASS PROBLEM F*CK THIS SHIT!!
#include <bits/stdc++.h>

using namespace std;
#define FOR(i, n) for(int i = 0; i < (n); ++i)
using ll = long long;

struct tb
{
    string nome;
    ll gm = 0LL, gs = 0LL;
    ll pontos = 0LL, j = 0LL;
};

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(NULL);

    array<tb, 28> times;

    int t, g;
    int gA, gB;
    string tA, tB;

    cin >> t >> g;
    while(t > 0 && g > 0)
    {
        map<string, int> name_hash;
        cout.precision(2);

        ll tp = 0;

        FOR(i, t)
        {
            cin >> times[i].nome;
            // cout << times[i].nome << '\n';
            times[i].gm = times[i].gs = times[i].pontos = times[i].j = 0LL;
            name_hash[times[i].nome] = i;
        }

        FOR(i, g)
        {
            cin >> tA >> gA;
            // cout << "Time: " << tA << " marcou " << gA << '\n';
            cin.ignore(3, '-');
            cin >> gB >> tB;
            // cout << "Time: " << tB << " marcou " << gB << '\n';
            auto& a = times[name_hash[tA]];
            auto& b = times[name_hash[tB]];
            a.j++; b.j++;           // jogos
            a.gm += gA; b.gm += gB; // gols marcados
            a.gs += gB; b.gs += gA; // gols sofridos
            if(gA > gB)
            {
                a.pontos += 3;
                tp += 3;
            }
            else if(gA < gB)
            {
                b.pontos += 3;
                tp += 3;
            }
            else
            {
                a.pontos += 1;
                b.pontos += 1;
                tp += 2;
            }
        }
        sort(times.begin(), times.begin() + t, [](const tb& a, const tb& b){
            if(a.pontos == b.pontos)
            {
                auto gA = a.gm-a.gs;
                auto gB = b.gm-b.gs;
                if(gA == gB)
                {
                    if(a.gm == b.gm)
                        return strcasecmp(a.nome.c_str(), b.nome.c_str()) <= 0;
                    else
                        return a.gm > b.gm;
                }
                else
                    return gA > gB;
            }
            else
                return a.pontos > b.pontos;
        });

        FOR(i, t)
        {
            const auto& tm = times[i];
            cout << tm.nome << " " << tm.pontos << " "  << tm.j << " " << tm.gm << " " << tm.gs << " " << tm.gm - tm.gs << " ";
            if(tm.j > 0)
            {
                auto f = ((100.0 * double(tm.pontos))/tp);
                cout << fixed << f << '\n';
            }
            else
                cout << "N/A\n";
        }
        cout << '\n';

        cin >> t >> g;
    }
    return 0;
}