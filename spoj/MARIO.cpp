// STATUS: ACCEPTED
#include <bits/stdc++.h>

using namespace std;

using ll = unsigned long long;

int main()
{
    array<ll, 100000> lockers;

    ios_base::sync_with_stdio(false);
    cin.tie(NULL);

    int n, l;
    cin >> n >> l;
    while(n > 0 && l > 0)
    {
        ll m = 100000000;
        for(int i = 0; i < l; ++i)
            cin >> lockers[i];
        for(int i = 0; i < l; ++i)
        {
            // the last locker number to make a sequence from
            // l[i] to l[i] + n - 1
            auto e = lockers[i] + n - 1;

            auto lb = lower_bound(lockers.cbegin(), lockers.cbegin() + l, e);
            auto _m = n - (lb - &lockers[i] + 1);
            // in this case, the last item requires an additional swap
            if(*lb != e) _m++;
            if(_m < m)
                m = _m;
        }
        cout << m << '\n';
        cin >> n >> l;
    }
    return 0;
}