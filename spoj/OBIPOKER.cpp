// STATUS: ACCEPTED
#include <bits/stdc++.h>

using namespace std;

enum hand_t
{
    nothing,
    one_pair,
    two_pairs,
    three_of_a_kind,
    four_of_a_kind,
    full_house,
    straight
};

int has_straight(const map<int, int>& m)
{
    auto b = m.cbegin();
    auto e = m.cend();
    int v = b->first;
    int f = v;
    b++;
    for(; b != e; b++)
    {
        if(v+1 != b->first)
            return -1;
        v = b->first;
    }
    return f;
}

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(NULL);

    int n, card;

    cin >> n;
    for(int i = 0; i < n; ++i)
    {
        cout << "Teste " << i+1 << '\n';
        hand_t hand = nothing;
        int c1, c2;
        c1 = c2 = 0;
        map<int, int> m;
        for(int j = 0; j < 5; ++j)
        {
            cin >> card;
            auto r = m.emplace(card, 1);
            if(!r.second)
            {
                (*r.first).second++;
                switch ((*r.first).second)
                {
                case 2: // detects pairs
                    if(hand == nothing)
                    {
                        hand = one_pair;
                        c1 = r.first->first; // card number
                    }
                    else if(hand == one_pair)
                    {
                        hand = two_pairs;
                        c2 = r.first->first; // card number
                    }
                    else if(hand == three_of_a_kind)
                    {
                        hand = full_house;
                        c2 = r.first->first; // card number
                    }
                    break;
                case 3: // detects 3 cards
                    if(hand == one_pair)
                        hand = three_of_a_kind;
                    else if(hand == two_pairs)
                        hand = full_house;
                    else
                        cout << "PROBLEMA\n";
                    break;
                case 4: // detect four cards
                    hand = four_of_a_kind;
                    break;
                
                default:
                    break;
                }
                
            }
        }
        auto st = has_straight(m);
        if(hand == nothing)
        {
            if(st > 0)
                cout << 200 + st << '\n';
            else
                cout << "0\n";
        }
        else if(hand == one_pair)
            cout << c1 << '\n';
        else if(hand == two_pairs)
        {
            if(c1 < c2)
                swap(c1, c2);
            cout << (3*c1) + 2*c2 + 20 << '\n';
        }
        else if(hand == three_of_a_kind)
            cout << c1 + 140 << '\n';
        else if(hand == four_of_a_kind)
            cout << c1 + 180 << '\n';
        else if(hand == full_house)
        {
            if(m[c1] == 3)
                cout << c1 + 160 << '\n';
            else
                cout << c2 + 160 << '\n';
        }
        if(i < n-1)
            cout << '\n';
    }
}