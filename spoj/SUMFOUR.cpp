// STATUS: ACCEPTED
#include <bits/stdc++.h>

using namespace std;

using ll = long long;

array<ll, 4000> a, b, c, d;
array<ll, 4000*4000> cPlusd;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(NULL);

    int n;
    ll count = 0;
    cin >> n;

    for(int i = 0; i < n; ++i)
    {
        cin >> a[i] >> b[i] >> c[i] >> d[i];
    }

    for(int i = 0; i < n; ++i)
        for(int j = 0; j < n; ++j)
        {
            cPlusd[i*n+j] = c[i] + d[j]; 
        }

    sort(cPlusd.begin(), cPlusd.begin() + (n*n));

    for(int i = 0; i < n; ++i)
        for(int j = 0; j < n; ++j)
        {
            auto bounds = equal_range(cPlusd.cbegin(), cPlusd.cbegin() + (n*n), -(a[i]+b[j]));
            count += bounds.second - bounds.first;
        }
    cout << count << '\n';
}