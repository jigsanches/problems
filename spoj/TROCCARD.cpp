// STATUS = ACCEPTED

#include <bits/stdc++.h>
#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(NULL);

    int n, m, card;

    cin >> n >> m;
    while(n != 0 || m != 0)
    {
        unordered_set<int> alice;
        alice.reserve(n);

        int _n, _m = m;
        int dupes = 0;
        int last_card = -1;

        for (int i = 0; i < n; ++i)
        {
            cin >> card;
            alice.insert(card);
        }
        // numero de cartas distintas da alice
        _n = alice.size();
        for(int i = 0; i < m; ++i)
        {
            cin >> card;
            if(card == last_card)
            {
                _m--;
                continue;
            }
            last_card = card;
            auto e = alice.find(card);
            if(e != alice.end()) // tem a carta
                dupes++;
        }
        cout << min(_n-dupes, _m-dupes) << '\n';
        cin >> n >> m;
    }
    return 0;
}