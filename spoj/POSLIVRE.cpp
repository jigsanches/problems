// STATUS: ACCEPTED
#include <bits/stdc++.h>

using namespace std;

char m[512][512];
int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(NULL);

    int w, h, n;
    int x1, x2, y1, y2;
    cin >> w >> h >> n;
    while(w > 0 && h > 0)
    {
        int tiles = w * h;
        for(int i = 0; i < h; ++i)
            for(int j = 0; j < w; ++j)
                m[i][j] = 1;
        
        for(int i = 0; i < n; ++i)
        {
            cin >> x1 >> y1 >> x2 >> y2;
            if(x1 > x2) swap(x1, x2);
            if(y1 > y2) swap(y1, y2);

            for(int i = y1 - 1; i < y2; ++i)
                for(int j = x1 - 1; j < x2; ++j)
                {
                    if(m[i][j])
                        tiles--;
                    m[i][j] = 0;
                }
        }
        if(tiles > 0)
            if(tiles == 1)
                cout << "There is one empty spot.\n";
            else
                cout << "There are " << tiles << " empty spots.\n";
        else
            cout << "There is no empty spots.\n";
        cin >> w >> h >> n;
    }
    return 0;
}