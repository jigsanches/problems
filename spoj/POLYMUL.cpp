// STATUS: ACCEPTED
#include <bits/stdc++.h>

#define ll long long
#define ull unsigned ll

using namespace std;

using cd = complex<double>;
const double PI = acos(-1);

/**
 * @brief This function assumes @p a is a vector with 2^n size.
 * 
 * @param a vector to transform.
 * @param invert if true applies the inverse fft.
 */
void fft(vector<cd>& a, bool invert)
{
    auto n = a.size();
    if(n == 1)
        return;

    // h_n \gets n/2
    auto h_n = n >> 1;
    // compute A^0 and A^1
    vector<cd> a0(h_n), a1(h_n);
    for(ull i = 0; i < h_n; ++i)
    {
        a0[i] = a[2*i];    // A^0_i = A[i] \forall i that is even.
        a1[i] = a[2*i+1];  // A^1_i = A[i] \forall i that is odd.
    }

    fft(a0, invert);
    fft(a1, invert);

    // represents the e^{2\pi / n}
    auto ang = 2 * PI / n * (invert ? -1 : 1);
    // w  : accumulates the powers of w^n, starting with w^0 = 1.
    // wn : represents w^1, adding 1 to the exponent of w everytime w *= wn.
    cd w(1.0), wn(cos(ang), sin(ang));
    // after computing A^0 and A^1, join them to produce A.
    for(ull i = 0; i < h_n; ++i)
    {
        // first half  : A[1..n/2]   = A^0[1..n/2] + (w^{0..n/2 - 1} * A^1[1..n/2]) 
        a[i] = a0[i] + w * a1[i];
        // second half : A[n/2+1..n] = A^0[1..n/2] - (w^{0..n/2 - 1} * A^1[1..n/2]) 
        a[i + h_n] = a0[i] - w * a1[i];
        
        if(invert)
        {
            // in this case, at every recursion level (total of ln(n)), the
            // elements get divided by 2. Resulting in 1/n * a[i] for each of
            // them, just like intended by the inverse transform.
            a[i] /= 2;
            a[i + h_n] /= 2;
        }

        // w^k * w^1 = w^{k+1}
        w *= wn;
    }
}

void multiply(const vector<ll>& a, const vector<ll>& b, vector<ll>& result)
{
    vector<cd> fa(a.cbegin(), a.cend()), fb(b.cbegin(), b.cend());
    ull n = 1ULL;
    while (n < a.size() + b.size())
        n <<= 1;
    // with this, we are sure the vectors have size as a power of 2.
    fa.resize(n);
    fb.resize(n);

    fft(fa, false);
    fft(fb, false);
    for(ull i = 0; i < n; ++i)
        fa[i] *= fb[i];

    // inverse transform FA
    fft(fa, true);

    result.resize(n);
    for(ull i = 0; i < n; ++i)
        result[i] = round(fa[i].real());
}

int main()
{
    ios_base::sync_with_stdio(false); // fast IO
    cin.tie(NULL);
    
    int t, n;
    
    cin >> t;
    while (t-- > 0)
    {
        cin >> n;
        vector<ll> a(n+1), b(n+1);
        for(ll i = n; i >= 0; --i)
        {
            cin >> a[i];
        }
        for(ll i = n; i >= 0; --i)
        {
            cin >> b[i];
        }
        vector<ll> r;
        multiply(a, b, r);
        for(ll i = n+n; i > 0; --i)
        {
            cout << r[i] << ' ';
        }
        cout << r[0];
        cout << '\n';
    }
    return 0;
}