# -*- coding: utf-8 -*-

from sys import stdin

def findBrackets(text : str, start):
  for i in range(start, len(text)):
    if(text[i] == ']' or text[i] == '['): return i
  return len(text)

while True:
  try:
    text = input()
    t = list()
    end = findBrackets(text, 0)
    start = 0
    t.append(text[start:end])
    start = end + 1
    
    if end != len(text):
      putOnEnd = False if text[end] == '[' else True

    while end != len(text):
      end = findBrackets(text, start)
      if putOnEnd: t.append(text[start : end])
      else: t.insert(0, text[start : end])
      start = end + 1
      if end != len(text):
        putOnEnd = False if text[end] == '[' else True
    print(''.join(t))
  except EOFError:
    break