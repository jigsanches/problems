# -*- coding: utf-8 -*-

num = 1
count = 1
n = int(input())
while num != n/2 + 1:
  count += 1
  num *= 2
  if num > n: num -= n + 1
print(count)