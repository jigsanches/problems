# STATUS: ACCEPTED

n = int(input())
while n > 0:
    hits = 0
    t = int(input())
    heights = [int(i) for i in input().split()]
    moves = [i for i in input()]

    for i in range(len(heights)):
        if moves[i] == 'S' and heights[i] < 3:
            hits += 1
        elif moves[i] == 'J' and heights[i] > 2:
            hits += 1
    print(hits)
    n -= 1