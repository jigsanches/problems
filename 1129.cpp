// STATUS: ACCEPTED

#include <bits/stdc++.h>
using namespace std;

#define FOR(n) for(int i = 0; i < (n); ++i)

int main()
{
  ios_base::sync_with_stdio(false);
  cin.tie(NULL);

  int n, ans;
  cin >> n;
  while(n)
  {
    FOR(n)
    {
      char r = 0;
      for(int j = 0; j < 5; ++j)
      {
        cin >> ans;
        if(ans <= 127)
        {
          if(r != 0)
            r = '*';
          else
            r = 'A' + j;
        }
      }
      if (r == 0) r = '*';
      cout << r << '\n';
    }

    cin  >> n;
  }
}