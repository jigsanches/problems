// STATUS: ACCEPTED
#include <bits/stdc++.h>

using namespace std;


auto graph = vector<vector<int>>(26, vector<int>(26, 0));

int main() {
  int t, v, e;
  cin >> t;

  for(int i = 0; i < t; ++i)
  {
    cout << "Case #" << i+1 << ":" << endl;
    int c = 0;

    cin >> v >> e;
    char a, b;
    int u, n;
    for(int j = 0; j < e; ++j)
    {
      cin >> a >> b;
      u = a - 'a';
      n = b - 'a';
      
      graph[u][n] = 1;
      graph[n][u] = 1;
    }

    stack<int> s;
    vector<int> comp;
    vector<int> visited(v, 0);
    for(int l = 0; l < v; ++l)
    {
      bool is_comp = false;
      if(!visited[l])
      {
        s.push(l);
        comp.assign(v, 0);
        c++;
        is_comp = true;
      }

      while(!s.empty())
      {
        u = s.top(); s.pop();
        comp[u] = visited[u] = 1;

        for(n = 0; n < v; ++n)
        {
          if(graph[u][n] && !visited[n])
          {
            s.push(n);
          }
        }
      }

      if(is_comp)
      {
        for(int j = 0; j < v; ++j)
        {
          if(comp[j])
            cout << (char)(j + 'a') << ",";
        }
        cout << endl;
      }
    }

    graph.assign(26, vector<int>(26, 0));
    cout << c << " connected components\n\n";
  }
}
