// STATUS = ACCEPTED
#include <bits/stdc++.h>

using namespace std;

int main(int argc, char const *argv[])
{
    ios_base::sync_with_stdio(false);
    cin.tie(NULL);

    double a, b, c;

    cin >> a >> b >> c;
    constexpr auto eps = numeric_limits<double>::epsilon();

    auto delta = (b * b) - 4.0 * a * c;
    if((delta <= eps) || (a <= eps))
    {
        cout << "Impossivel calcular\n";
    }
    else
    {
        cout.precision(5);
        cout << "R1 = " << fixed << (-b + sqrt(delta))/(2 * a) << '\n';
        cout << "R2 = " << fixed << (-b - sqrt(delta))/(2 * a) << '\n';
    }
    

    return 0;
}
