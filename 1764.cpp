#include <bits/stdc++.h>
using namespace std;
// STATUS: ACCEPTED
using edge = pair<int, int>;

int root[40000]; // root of i
vector<int> _size; // _size of trees rooted at i
vector<pair<int, edge>> edge_array(50000);


auto _find(int i){
    while(i != root[i]){
        root[i] = root[root[i]]; // flattens tree
        i = root[i];
    }
    return i;
}

auto _union(int p, int q){
    int i = _find(p); // get root
    int j = _find(q); // get root
    if(_size[i] < _size[j]){
        root[i] = j;
        _size[j] += _size[i];
    }
    else{
        root[j] = i;
        _size[i] += _size[j];
    }
    return;
}

int main(){
    ios_base::sync_with_stdio(false); // fast IO
    cin.tie(NULL);

    int n, m;
    int u, v;
    unsigned total_weight, w;
    cin >> n >> m;
    while(n != 0){
        total_weight = 0;
        _size.assign(n, 1);
        for(int i = 0; i < n; ++i)
            root[i] = i;
        for(int i = 0; i < m; ++i){
            cin >> u >> v >> w;
            edge_array[i] = pair<int, edge>(w, edge(u, v));
        }
        sort(edge_array.begin(), edge_array.begin()+m);
        for(int i = 0; i < m; ++i){
            auto e = edge_array[i];
            auto root_i = _find(e.second.first);
            auto root_j = _find(e.second.second);
            if(root_i != root_j){
                total_weight += e.first;
                _union(root_i, root_j);
            }
        }
        cout << total_weight << '\n';
        cin >> n >> m;
    }
    return 0;
}