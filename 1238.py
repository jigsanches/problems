# STATUS: ACCEPTED

n = int(input())

for i in range(n):
    # separa a linha do input nos espaços em branco
    # e coloca a primeira em s1 e segunda em s2
    s1, s2 = input().split()
    
    menor = len(s1)
    if len(s2) < menor:
        menor = len(s2)

    maior_string = s1
    if len(s2)>len(s1):
        maior_string = s2


    resultado = ''
    # 0,..., menor-1.
    for j in range(menor):
        # s1[0] + s2[0], s1[1], s2[1], ....
        resultado = resultado + s1[j] + s2[j]
    
    resultado = resultado + maior_string[menor:]

    print(resultado)