from operator import itemgetter

n = int(input())
while n > 0:
    lines = []
    d = dict()
    for i in range(n): lines.append(input())
    text = ''.join(lines)
    total = len(text) - 1
    for i in range(total):
        dig = text[i:i+2]
        if d.get(dig) == None: d[dig] = 1
        else: d[dig] = d[dig] + 1
    tuples = [(v, k) for k, v in d.items()]
    tuples.sort(key=itemgetter(1))
    tuples.sort(key=itemgetter(0), reverse=True)

    for i in tuples[0:5]:
        val = i[0]/float(total)
        print(f'{i[1]} {i[0]} {val:.6f}')
    n = int(input())
    print()
