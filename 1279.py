from sys import stdin
# STATUS: ACCEPTED
def div(i, n = 2):
    return i % n == 0

nums = [int(i) for i in stdin.readlines()]

for k in range(len(nums) - 1):
    i = nums[k]
    leap_year = (div(i, 4) and not div(i, 100)) or (div(i, 400))
    huluculu = div(i, 15)
    bulukulu = div(i, 55) and leap_year
    if leap_year or huluculu:
        if leap_year: print("This is leap year.")
        if huluculu : print("This is huluculu festival year.")
        if bulukulu : print("This is bulukulu festival year.")
    else:
        print("This is an ordinary year.")
    print()

i = nums[-1]
leap_year = (div(i, 4) and not div(i, 100)) or (div(i, 400))
huluculu = div(i, 15)
bulukulu = div(i, 55) and leap_year
if leap_year or huluculu:
    if leap_year: print("This is leap year.")
    if huluculu : print("This is huluculu festival year.")
    if bulukulu : print("This is bulukulu festival year.")
else:
    print("This is an ordinary year.")