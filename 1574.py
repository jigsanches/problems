n = int(input())
for k in range(n):
    i = int(input())
    pos = 0
    commands = list()
    for it in range(i):
        cmd = input()
        if cmd == "LEFT":
            commands.append(-1)
            pos += -1
        elif cmd == "RIGHT":
            commands.append(1)
            pos += 1
        else:
            idx = int(cmd.split()[-1]) - 1
            pos += commands[idx]
            commands.append(commands[idx])
    print(pos)