n = int(input())
for k in range(n):
    x, y, z, a, b, c = [int(i) for i in input().split()]
    obj1 = [x, y, z]
    obj2 = [a, b, c]

    obj1.sort()
    obj2.sort()
    ok1 = (obj1[2] > obj2[1]) and (obj1[1] > obj2[0])
    ok2 = (obj2[2] > obj1[1]) and (obj2[1] > obj1[0])
    if ok1:
        if ok2: print(3)
        else: print(2)
    elif ok2:
        print(1)
    else: print(0)
