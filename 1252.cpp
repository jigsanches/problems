#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

int main()
{
    int n, r;
    cin >> n >> r;
    while(n != 0 && r != 0)
    {
        cout << n << " " << r << endl;
        auto v = vector<int>(n);
        for(int i = 0; i < n; ++i)
            cin >> v[i];
        stable_sort(
            v.begin(), 
            v.end(), 
            [r](int a, int b){
                if((a % r) == (b % r))
                {
                    bool aPar = a % 2 == 0;
                    bool bPar = b % 2 == 0;
                    if((aPar == true) && (bPar == true))
                        return a < b;
                    else if((aPar == false) && (bPar == false))
                        return a > b;
                    else
                        if(aPar == false)
                            return true;
                        else
                            return false;
                }
                else
                    return (a % r) < (b % r);
            }
        );
        for(int i = 0; i < n; ++i)
        {
            cout << v[i] << endl;
        }
        cin >> n >> r;
    }
    cout << "0 0" << endl;
    return 0;
}