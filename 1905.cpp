#include <iostream>
#include <queue>
// STATUS: ACCEPTED
using namespace std;

typedef struct tupla
{
    int x, y;
} tupla;


int cops_n_robbers(int board[5][5]){
    queue<tupla> q;
    if(board[0][0]) return 0;
    q.push({0, 0});
    while(!q.empty()){
        auto t = q.front();
        q.pop();
        board[t.x][t.y] = -1;
        if((t.x == 4) && (t.y == 4)) return 1;
        if(t.x < 4 && board[t.x+1][t.y] == 0) q.push({t.x+1, t.y}); //down
        if(t.x > 0 && board[t.x-1][t.y] == 0) q.push({t.x-1, t.y}); //up
        if(t.y < 4 && board[t.x][t.y+1] == 0) q.push({t.x, t.y+1}); //right
        if(t.y > 0 && board[t.x][t.y-1] == 0) q.push({t.x, t.y-1}); //left
    }
    return 0;
}

int main(){
    int n;
    int board[5][5];
    cin >> n;
    while(n --> 0){
        for(int i = 0; i < 5; ++i){
            scanf("%d%d%d%d%d", 
            &board[i][0],
            &board[i][1],
            &board[i][2],
            &board[i][3],
            &board[i][4]
            );
        }
        if(cops_n_robbers(board))
            cout << "COPS\n";
        else
            cout << "ROBBERS\n";
    }
    return 0;
}