import math
from fractions import Fraction
import collections
# STATUS: ACCEPTED
while True:
    try:
        i = input()
        counter = collections.Counter(i)
        res = math.factorial(len(i))
        d = 1
        for c in counter.values():
            d = d * math.factorial(c)
        res = Fraction(res, d)
        print(res.__mod__(1000000007))
    except EOFError:
        break