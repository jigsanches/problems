# -*- coding: utf-8 -*-

while True:
  try:
    line = input()
    mask = True
    modLine = str()
    for i in range(len(line)):
      if(line[i].isalpha()):
        modLine += line[i].upper() if mask else line[i].lower()
        mask = not mask
      else:
        modLine += line[i]
      
    print(modLine)
  except EOFError:
    break