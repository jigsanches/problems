# STATUS: ACCEPTED
d = int(input())
p, q, r, s, x, y = (int(i) for i in input().split())
p_i, p_j = (int(i) for i in input().split())

sum = 0
p_pi = p* p_i
s_pj = s* p_j
for i in range(1, d+1):
    sum += ((p_pi+q*i)%x) * ((r*i+ s_pj)%y)
print(sum)
