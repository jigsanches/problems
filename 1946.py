from math import factorial as f
from fractions import Fraction as F
from decimal import Decimal as D
# STATUS: ACCEPTED
n = int(input())
n -= 1
a = D(100*f(n)) / D(f(n/2)**2)
b = D(2**n)
print("%.2f" % (a / b))