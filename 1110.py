# -*- coding: utf-8 -*-

n = int(input())
while n != 0:
  l = list(range(1, n+1)) if n > 1 else [1]
  l.reverse()
  removed = list()
  while len(l) != 1:
    removed.append(l.pop())
    num = l.pop()
    l.insert(0, num)
  if n > 1:
    print("Discarded cards:", end=' ') 
  else:
    print("Discarded cards:")

  for i in range(len(removed) - 1):
    print("%d, " % removed[i], end='')

  if n > 1 : print("%d" % removed[-1])

  print("Remaining card: %d" % l[0])
  n = int(input())