#include <iostream>

using namespace std;

int main()
{
    int maiorSoma = 0, maximoAteAqui = 0;
    int custo;
    int n, num;
    int vetor[51];

    while(!cin.eof())
    {
        maiorSoma = 0;
        maximoAteAqui = 0;
        cin >> n >> custo;
        for(int i = 0; i < n; ++i)
        {
            cin >> num;
            num -= custo;
            vetor[i] = num;
        }
        for(int i = 0; i < n; ++i)
        {
            maximoAteAqui += vetor[i];
            if(maximoAteAqui < 0)   maximoAteAqui = 0;
            if(maiorSoma < maximoAteAqui) maiorSoma = maximoAteAqui;
        }
        cout << maiorSoma << endl;
    }
    return 0;
}