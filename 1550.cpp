#include <bits/stdc++.h>
#include <limits>
// STATUS: ACCEPTED
#define MAX 10001

using namespace std;
using dupla = pair<int,int>;

int inv[MAX];

int main(){
    int n, a, b, flipped;
    int d;
    vector<int> depth;
    dupla t;
    int div;
    for(int i = 0; i < MAX; ++i){
        div = i;
        flipped = 0;
        while(div != 0){
            flipped = flipped*10 + div%10;
            div /= 10;
        }
        inv[i] = flipped;
    }
    cin >> n;
    for(int i = 0; i < n; ++i){
        cin >> a >> b;
        depth.assign(MAX, INT_MAX);
        priority_queue<dupla, vector<dupla>, greater<dupla>> s;
        s.push(dupla(0, a)); // depth primeiro pq operador < de pair compara o first antes do second
        depth[a] = 0;
        while(!s.empty()){
            t = s.top(); s.pop();
            if(t.second < MAX){
                if(t.first+1 < depth[t.second+1] && t.second+1<MAX) {
                    depth[t.second+1] = t.first+1;
                    s.push(dupla(t.first+1, t.second+1));
                }
                if(t.first+1 < depth[inv[t.second]] && inv[t.second] < MAX){
                    depth[inv[t.second]] = t.first+1;
                    s.push(dupla(t.first+1, inv[t.second]));
                }
            }
        }
        cout << depth[b] << '\n';
    }
    return 0;
}