#include <bits/stdc++.h>
// STATUS: ACCEPTED
using namespace std;

bool dfs(int adj[20][20], int v[20], int i, int d){
    if(v[i]) return false;
    bool ret = false;
    v[i] = 1;
    for(int j = 0; j < 20; ++j){
        if(adj[i][j]){
            ret = true;
            for(int w = 0; w < d; ++w)
                printf("  ");
            printf("%d-%d", i, j);
            if(!v[j]){
                printf(" pathR(G,%d)", j);
            }
            printf("\n");
            dfs(adj, v, j, d+1);
        }
    }
    return ret;
}

int main(){
    int adj[20][20];
    int n, v, e;
    int i, j;
    cin >> n;
    for(int l = 0; l < n; ++l){
        cin >> v >> e;
        int visited[20] = {0};

        memset(adj, 0, sizeof(adj));
        for(int m = 0; m < e; ++m){
            cin >> i >> j;
            adj[i][j] = 1;
        }
        printf("Caso %d:\n", l+1);
        for(int i = 0; i < v; ++i){
            if(dfs(adj, visited, i, 1))
            cout << '\n';
        }
    }
    return 0;
}