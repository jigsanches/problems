#include <bits/stdc++.h>
// STATUS: ACCEPTED
#define endl '\n'

using namespace std;
using dupla = pair<int,int>;

int main(){
    ios_base::sync_with_stdio(false); // fast IO
    cin.tie(NULL);
    int n, m, u, v, weight;
    int queries;
    cin >> n >> m;
    int adj[500][500];
    while(n > 0){
        for(int i = 0; i < n; ++i){
            for(int j = 0; j < n; ++j)
                adj[i][j] = 3000;
            adj[i][i] = 0;
        }
        for(int i = 0; i < m; ++i){
            cin >> u >> v >> weight;
            u--; v--;
            if(adj[v][u] < 3000){
                adj[u][v] = 0;
                adj[v][u] = 0;
            }
            else{
                adj[u][v] = weight;
            }
        }
        for(int k = 0; k < n; ++k){
            for(int i = 0; i < n; ++i){
                for(int j = 0; j < n; ++j){
                    if(adj[i][k] + adj[k][j] < adj[i][j])
                        adj[i][j] = adj[i][k] + adj[k][j];
                }
            }
        }
        cin >> queries;
        while(queries-->0){
            cin >> u >> v;
            u--; v--;
            if(adj[u][v] == 3000){
                cout << "Nao e possivel entregar a carta\n";
            }
            else{
                cout << adj[u][v] << endl;
            }
        }
        cout << endl;
        cin >> n >> m;
    }
}