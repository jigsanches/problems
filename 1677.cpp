#include <bits/stdc++.h>
// STATUS: ACCEPTED
#define endl '\n'
using namespace std;

// graph and reverse graph
vector<vector<int>> g;
vector<vector<int>> gr;
vector<vector<int>> condensed_g_adj, condensed_g_members;
vector<int> order, component;
vector<bool> should_print;
vector<bool> used; 
int component_count = 0;

void dfs_post_order(int i){
    used[i] = true;
    for(auto j : g[i]){
        if(!used[j]){
            dfs_post_order(j);
        }
    }
    order.emplace_back(i);
}

void dfs_find_component(int i){
    used[i] = true;
    condensed_g_members[component_count].push_back(i);
    component[i] = component_count;
    for(auto j : gr[i]){
        if(used[j]) {//ja pertence a uma componente
            if(component_count != component[j]){
                condensed_g_adj[component[j]].emplace_back(component_count); // ligo componente de j com a componente de i
            }
        }
        else{
            dfs_find_component(j);
        }
    }
}

int main (){
    ios_base::sync_with_stdio(false); // fast IO
    cin.tie(NULL);
    int n, m, u, v;
    cin >> n;
    while(n != 0){
        component_count = 0;
        cin >> m; // arestas
        int go_from_here =-1;
        condensed_g_adj.clear();
        condensed_g_members.clear();
        order.clear();
        g.assign(n, vector<int>());
        gr.assign(n, vector<int>());
        should_print.assign(n, false);
        component.assign(n, -1);
        if(m == 0){
            cout << 1;
            for(int i = 2; i < n+1; ++i){
                cout << ' ' << i;
            }
        }
        else{
            for(int i = 0; i < m; ++i){
                cin >> u >> v;
                u--; v--;
                g[u].emplace_back(v);
                gr[v].emplace_back(u);
            }
            used.assign(n, false);
            for(int i = 0; i < n; ++i){
                if(!used[i]) dfs_post_order(i);
            }
            used.assign(n, false);
            for(int i = 0; i < n; ++i){
                int w = order[n-i-1];
                if(!used[w]){
                    condensed_g_adj.push_back(vector<int>());
                    condensed_g_members.push_back(vector<int>());
                    dfs_find_component(w);
                    component_count++;
                }
            }
            for(int i = 0; i < condensed_g_adj.size(); ++i){
                if(condensed_g_adj[i].size() == 0){
                    for(auto j : condensed_g_members[i])
                        should_print[j] = true;
                }
            }
            for(int i = 0; i < n; ++i){
                if(should_print[i]){
                    go_from_here = i+1;
                    cout << i+1;
                    break;
                }
            }
            if(go_from_here != -1)
                for(int i = go_from_here; i < n; ++i){
                    if(should_print[i]){
                        cout << ' ' << i+1;
                    }
                }
        }
        cout << endl;

        cin >> n;
    }
    return 0;
}