fibs = [1, 2]
for i in range(2, 513): fibs.append(fibs[i-1] + fibs[i-2])
# STATUS: ACCEPTED
a, b = (int(i) for i in input().split())
while a != 0 or b != 0:
    i = 0
    while fibs[i] < a:
        i += 1
    j = i
    while fibs[j] <= b:
        j += 1
    print(j - i)
    a, b = (int(i) for i in input().split())