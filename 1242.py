# STATUS: ACCEPTED
matches = ['BS', 'SB', 'CF', 'FC']
while True:
    try:
        count = 0
        line = input()
        while len(line) > 1:
            checked = False
            for base in matches:
                idx = line.find(base)
                if idx != -1:
                    checked = True
                    count += 1
                    line = line[0:idx] + line[idx+2:]
            if not checked: break
        print(count)

    except EOFError: break