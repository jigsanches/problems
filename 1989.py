# STATUS: ACCEPTED
n, m = (int(i) for i in input().split())
while n != -1:
    time = [int(i)*m for i in input().split()]
    sum = time[0]
    for i in range(1, n):
        time[i] += time[i-1]
        sum += time[i]
    print(sum)
    n, m = (int(i) for i in input().split())