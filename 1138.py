# STATUS: ACCEPTED
# Solved thanks to https://shorturl.at/lrPR1

def f_i(n, i):
    l = len(n)
    a = int(n[0])
    if l == 1:
        if a < i: return 0
        else: return 1
    b = n[1:]
    k = l-1
    if a < i:
        return a * k * (10**(k-1)) + f_i(b, i)
    elif a == i:
        return a * k * (10**(k-1)) + 1 + f_i(b, i) + int(b)
    else:
        return (a * k + 10) * (10**(k-1)) + f_i(b, i)

def t_n(n):
    k = len(n) - 1
    a = int(n[0])
    b = int(n[1:]) if len(n) > 1 else 0
    return (k+1) * (b + 1 + (a - 1) * 10**k) + sum([(9*s*(10**(s-1))) for s in range(1, k+1)])

a, b = (i for i in input().split())
while a != '0' and b != '0':
    a = str(int(a)-1)
    a_i = [f_i(a, i) for i in range(1, 10)]
    b_i = [f_i(b, i) for i in range(1, 10)]
    a_i.insert(0, t_n(a) - sum(a_i))
    b_i.insert(0, t_n(b) - sum(b_i))
    print(*[b_i[i]-a_i[i] for i in range(10)])
    a, b = (i for i in input().split())
