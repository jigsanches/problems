#include <bits/stdc++.h>
using namespace std;
// STATUS: ACCEPTED
using edge = pair<int, int>;
using ull = unsigned long long;

int root[10001]; // root of i
vector<int> _size; // _size of trees rooted at i
vector<pair<ull, edge>> edge_array(1000001);

auto _find(int i){
    while(i != root[i]){
        root[i] = root[root[i]]; // flattens tree
        i = root[i];
    }
    return i;
}

auto _union(int p, int q){
    int i = _find(p); // get root
    int j = _find(q); // get root
    if(_size[i] < _size[j]){
        root[i] = j;
        _size[j] += _size[i];
    }
    else{
        root[j] = i;
        _size[i] += _size[j];
    }
    return;
}

int main(){
    ios_base::sync_with_stdio(false); // fast IO
    cin.tie(NULL);
    int n, v, m = 0;
    ull w;
    int n_edges = 0;
    int i = 0;
    int root_i, root_j;
    ull weight = 0;
    cin >> n;
    _size.assign(n, 1);
    int n_components = n; // num_componentes = V - A
    for(int j = 0; j < n-1; ++j){
        cin >> n_edges;
        m += n_edges;
        while(n_edges-->0){
            cin >> v >> w;
            v--;
            edge_array[i++] = pair<ull, edge>(w, edge(j, v));
        }
    }
    for(int i = 0; i < n; ++i)
        root[i] = i;
    sort(edge_array.begin(), edge_array.begin()+m);
    for(int i = 0; i < m; ++i){
        auto e = edge_array[i];
        root_i = _find(e.second.first);
        root_j = _find(e.second.second);
        if(root_i != root_j){
            weight += e.first;
            n_components--;
            _union(root_i, root_j);
        }
    }
    printf("%d %llu\n", n_components, weight);
    return 0;
}