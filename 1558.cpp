#include <bits/stdc++.h>
// STATUS: ACCEPTED
using namespace std;

bool try_(int n){
    int sq = sqrt(n);
    for(int i = 0; i <= sq; ++i){
        for(int j = 0; j <= sq; ++j){
            if(i*i+j*j == n){
                return true;
            }
        }
    }
    return false;
}

int main(){
    int n;
    while(cin >> n){
        if(try_(n))
            cout << "YES\n";
        else{
            cout << "NO\n";
        }
    }
}