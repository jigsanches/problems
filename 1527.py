# -*- coding: utf-8 -*-

class UnionFind:
  def __init__(self, lvlarray):
    self.size = [1] * len(lvlarray)
    self.id = list(range(len(lvlarray)))
    self.guilds = list(lvlarray)
    self.lvlarray = lvlarray

  def find(self, i):
    while i != self.id[i]: 
      self.id[i] = self.id[self.id[i]]
      i = self.id[i]
    return i

  def union(self, p, q):
    i = self.find(p)
    j = self.find(q)
    if(self.size[i] < self.size[j]):
      self.id[i] = j
      self.size[j] += self.size[i]
      self.guilds[j] += self.guilds[i]
    else:
      self.id[j] = i
      self.size[i] += self.size[j]
      self.guilds[i] += self.guilds[j]

# n = jogadores
# m = acoes

n, m = (int(i) for i in input().split())
while n != 0 and m != 0:
  wins = 0
  lvls = [int(i) for i in input().split()]
  unionFind = UnionFind(lvls)
  for i in range(m):
    op, p1, p2 = (int(j) for j in input().split())
    p1 -= 1
    p2 -= 1
    if op == 1:
      unionFind.union(p1, p2)
    else:
      fp1 = unionFind.find(p1)
      fp2 = unionFind.find(p2)
      fRafa = unionFind.find(0)
      if(unionFind.guilds[fp1] > unionFind.guilds[fp2] and fRafa == fp1): wins += 1
      if(unionFind.guilds[fp2] > unionFind.guilds[fp1] and fRafa == fp2): wins += 1
  print(wins)
  n, m = (int(i) for i in input().split())