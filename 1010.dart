import 'dart:io';

void main()
{
  double total = 0.0;
  var line1 = stdin.readLineSync()!             // read from stdin
                .split(' ')                     // split the input on whitespaces
                .map((e) => double.parse(e))    // map each value to a double
                .toList();                      // turn the result into a list
  var line2 = stdin.readLineSync()!
                .split(' ')
                .map((e) => double.parse(e))
                .toList();
  
  total = line1[1] * line1[2] + line2[1] * line2[2];
  print('VALOR A PAGAR: R\$ ${total.toStringAsFixed(2)}');
}