# STATUS: ACCEPTED
size_mapping = {
    'P': 0, 'M': 1, 'G': 2
}
r_size_mapping = {
    0: 'P', 1: 'M', 2: 'G'
}
n = int(input())
while n > 0:
    arr = []
    for i in range(n):
        name = input()
        cor, tam = (i for i in input().split())
        arr.append((cor, size_mapping[tam], name))
    arr.sort()
    for l in arr:
        print(str(l[0] + ' ' + r_size_mapping[l[1]] + ' ' + l[2]))
    n = int(input())
    if n != 0: print()