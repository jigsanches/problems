#!/bin/bash
# script to compile Kotlin Files
# can be extended to compile cpp and c files

filename=$(basename -- "$1")
extension="${filename##*.}"
filename="${filename%.*}"

kotlinc $1 -include-runtime -d $filename.jar