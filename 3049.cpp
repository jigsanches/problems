#include <bits/stdc++.h>
using namespace std;
// STATUS: ACCEPTED
int main(){
    ios_base::sync_with_stdio(false);
    cin.tie(NULL);

    int B, T;
    cin >> B >> T;
    if(B+T == 160)
        cout << "0" << endl;
    else if(B + T > 160)
        cout << "1" << endl;
    else
        cout << "2" << endl;
    return 0;
}