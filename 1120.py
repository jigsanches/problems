# STATUS: ACCEPTED
# -*- coding: utf-8 -*-

n, l = input().split()
while n != '0' and l != '0':
    c = l.replace(n, '')
    if len(c) == 0:
        print(0)
    else:
        print(int(c))
    n, l = input().split()