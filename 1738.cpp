#include <bits/stdc++.h>
// STATUS: ACCEPTED
#define endl '\n'
using namespace std;

int main(){
    ios_base::sync_with_stdio(false);
    cin.tie(NULL);
    int n;
    int u, v;
    string owners;
    cin >> n;
    while(n > 0){
        int adj[n][n];
        for(int i = 0; i < n; ++i)
            for(int j = 0; j < n; ++j)
                adj[i][j] = 0;
        cin >> u >> v;
        while(u > 0 && v > 0){
            u--;
            v--;
            cin >> owners;
            for(char c : owners) // every bit indicates if company can use edge
                adj[u][v] = adj[u][v] | (1 << (c - 'a'));
            cin >> u >> v;
        }
        for(int k = 0; k < n; ++k){
            for(int i = 0; i < n; ++i){
                for(int j = 0; j < n; ++j){
                    adj[i][j] = adj[i][j] | (adj[i][k] & adj[k][j]);
                }
            }
        }
        cin >> u >> v; // consultas
        while(u > 0){
            u--;
            v--;
            int mask = 0x1;
            string s;
            for(int k = 0; k < 27; ++k){
                if((adj[u][v] & mask) != 0){
                    s += ('a' + k);
                }
                mask <<= 1;
            }
            if(s.length() == 0)
                cout << "-\n";
            else
                cout << s << endl;;
            cin >> u >> v;
        }
        cout << endl;
        cin >> n;
    }
    return 0;
}