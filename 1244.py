# -*- coding: utf-8 -*-

n = int(input())
for i in range(n):
  l = input().split()
  l.sort(key=len, reverse=True)
  print(' '.join(l))