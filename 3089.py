n = int(input())
while n > 0:
    n_2 = 2*n
    arr = [int(i) for i in input().split()]
    maximum = minimum = arr[n-1] + arr[n] # sets both
    for i in range(n):
        num = arr[i] + arr[n_2 - i -1 ] #sums both ends
        if num < minimum: minimum = num
        elif num > maximum:maximum = num
    print(maximum, minimum)
    n = int(input())
