# -*- coding: utf-8 -*-

while True:
  try:
    stack = 0
    for c in input():
      if c == '(': stack += 1
      if c == ')':
        stack -= 1
        if stack < 0: break
    if stack != 0: print("incorrect")
    else: print("correct")
  except EOFError:
    break