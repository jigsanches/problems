from math import factorial as f
from fractions import Fraction as F
a, b = (int(i) for i in input().split())
# STATUS: ACCEPTED
t = list()
for i in range(a): t.append(list(input()))
x = 1 if t[0][0] == '#' else 0
fact = [1, 1]
for i in range(2, 55*55): fact.append(i * fact[i - 1])

soma = [[x]]
for i in range(1, b):
    x = 1 if t[0][i] == '#' else 0
    soma[0].append(x + soma[0][i-1])
for i in range(1, a):
    x = 1 if t[i][0] == '#' else 0
    soma.append([x + soma[i - 1][0]])
for i in range(1, a):
    for j in range(1, b):
        x = 1 if t[i][j] == '#' else 0
        x += soma[i][j-1] + soma[i-1][j] - soma[i-1][j-1]
        soma[i].append(x)

while True:
    try:
        x_a, y_a, x_b, y_b = (int(i) for i in input().split())
        x_a -= 2
        y_a -= 2
        x_b -= 1
        y_b -= 1
        area = (x_b - x_a) * (y_b - y_a)
        supDir = soma[x_a][y_b] if x_a >= 0 else 0
        infEsq = soma[x_b][y_a] if y_a >= 0 else 0
        supEsq = soma[x_a][y_a] if x_a >= 0 and y_a >= 0 else 0
        hashCount = soma[x_b][y_b] - supDir - infEsq + supEsq
        ans = (F(fact[area], fact[area-hashCount]*fact[hashCount])) - 1
        ans =  ans % 1000000007
        print(ans)
    except EOFError: break