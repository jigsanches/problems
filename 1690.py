# STATUS: ACCEPTED

t = int(input())
for c in range(t):
  n = int(input())
  nums = [int(i) for i in input().split()]

  nums.sort()
  # considerando o conjunto ordenado se podemos gerar numeros de 1 ate y um
  # numero x <= y+1 fara com que seja possivel gerar agora de 1 ate y+x
  # Por percorrer em ordem crescente, sabemos que se o x eh o menor
  # valor possivel e que se para este x > y+1, nenhum outro x poderia
  if nums[0] != 1:
    print('1')
  else:
    y = 1
    for i in range(1, n):
      if nums[i] <= y + 1:
        y += nums[i]
      else:
        break
    print(y+1)