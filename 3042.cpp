#include <bits/stdc++.h>
using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(NULL);
    int n, pos, touches;
    cin >> n;
    int line[3];
    while(n)
    {
        touches = 0;
        pos = 1;
        for(int i = 0; i < n; ++i)
        {
            cin >> line[0] >> line[1] >> line[2];
            if(line[pos] != 0)
            {
                // must switch lane
                for(int j = 0; j < 3; ++j){
                    if (!line[j]){
                        touches += abs(pos - j);
                        pos = j;
                        break;
                    }
                }
            }
        }
        cout << touches << '\n';
        cin >> n;
    }
    return 0;
}