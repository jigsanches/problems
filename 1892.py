# STATUS: ACCEPTED
def merge(arr, temp, left, mid, right):
    i = left     # Starting index of left subarray 
    j = mid + 1 # Starting index of right subarray 
    k = left     # Starting index of to be sorted subarray 
    inv_count = 0
    while i <= mid and j <= right: 
        if arr[i] <= arr[j]: 
            temp[k] = arr[i] 
            k += 1
            i += 1
        else: 
            temp[k] = arr[j] 
            inv_count += (mid-i + 1) 
            k += 1
            j += 1
    while i <= mid: 
        temp[k] = arr[i] 
        k += 1
        i += 1

    while j <= right: 
        temp[k] = arr[j] 
        k += 1
        j += 1

    for loop_var in range(left, right + 1): 
        arr[loop_var] = temp[loop_var] 

    return inv_count 
def _mergeSort(arr, temp, left, right):
    inv = 0
    if left < right:
        mid = (left + right) // 2
        inv += _mergeSort(arr, temp, left, mid)
        inv += _mergeSort(arr, temp, mid+1, right)
        inv += merge(arr, temp, left, mid, right)
    return inv

def mergeSort(arr, n):
    temp = [""]*n
    return _mergeSort(arr, temp, 0, n - 1)

while True:
    try:
        n = int(input())
        arr = []
        for i in range(n): arr.append(input())
        print(mergeSort(arr, n))
    except EOFError: break